#-*- coding: utf-8 -*-
import json
import urllib.parse
from urllib.parse import urlencode
import urllib.request
import re

class KeywordResolver(object):
	"""docstring for KeywordResolver"""

	to_regex_url = 'https://open-korean-text.herokuapp.com/normalize?'
	to_tokenize_url = 'https://open-korean-text.herokuapp.com/tokenize?'
	to_stem_url = 'https://open-korean-text.herokuapp.com/stem?'
	to_extract_url = 'https://open-korean-text.herokuapp.com/extractPhrases?'

	def __init__(self, target_text):
		parameters = {'text': target_text}
		self.target_text = self.parse_regex_text(urlencode(parameters))

	def parse_regex_text(self, target_text):
		reqURL = self.to_regex_url + target_text
		result = self.request_from(reqURL)
		return result['strings']

	def parse_tokenized_text(self):
		parameters = {'text' : self.target_text}
		reqURL = self.to_tokenize_url + urlencode(parameters)
		result = self.request_from(reqURL)
		return result

	def parse_stem_text(self):
		parameters = {'text' : self.target_text}
		reqURL = self.to_stem_url + urlencode(parameters)
		result = self.request_from(reqURL)
		return result

	def parse_extract_text(self):
		parameters = {'text' : self.target_text}
		reqURL = self.to_regex_url + urlencode(parameters)
		result = self.request_from(reqURL)
		return result['phrases']

	# def split_info(self, target):
	# 	print(target)
	# 	# p = re.compile(r"([가-힣A-Za-z0-9_\w]+)(\()([가-힣A-Za-z0-9_\w]+)(\))")
	# 	m = re.search(r"([가-힣A-Za-z0-9_\w]+)(\()([가-힣A-Za-z0-9_\w]+)(\))", target).group()
	# 	print(m)
	# 	keyword = m.group(1)
	# 	info = m.group(3).split(':')
	# 	target_type = info[0]
	# 	target_index = info[1].replace(' ', '').split(',')
	# 	target_index = list(int(target_index[0]), int(target_index[1]))

	# 	result = {
	# 		"word" : keyword,
	# 		"type" : target_type,
	# 		"target_index" : target_index
	# 	}
	# 	return result

	def split_info(self, target):
		s1 = target.split('(')
		keyword = s1[0]
		s2 = s1[1].split(':')
		target_type = s2[0]
		s3 = s2[1].replace(' ', '').replace(')', '').split(',')

		result = {
			"word" : keyword,
			"typeof" : target_type,
			"index" : [int(s3[0]), int(s3[1])]
		}
		return result

	def request_from(self, reqURL):
		req = urllib.request.Request(reqURL)
		json_result = str(urllib.request.urlopen(req).read().decode("utf-8"))
		return json.loads(json_result)