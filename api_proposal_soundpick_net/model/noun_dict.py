# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime
from flask_sqlalchemy import SQLAlchemy

from datetime import datetime, timedelta, date

from api_proposal_soundpick_net import db

class NounDictORM(db.Model):
    __tablename__ = "noun_dict"
    dict_id = Column(Integer, unique=True, primary_key=True)
    word = Column(String(30))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()