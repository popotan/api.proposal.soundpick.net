# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime
from flask_sqlalchemy import SQLAlchemy

from datetime import datetime, timedelta, date

from api_proposal_soundpick_net import db

class KeywordListORM(db.Model):
    __tablename__ = "keyword_list"
    keyword_id = Column(Integer, unique=True, primary_key=True)
    input_keyword = Column(String(100))
    output_keyword = Column(String(100))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()