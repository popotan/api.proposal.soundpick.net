# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime, Boolean
from flask_sqlalchemy import SQLAlchemy

from datetime import datetime, timedelta, date

from api_proposal_soundpick_net import db

class SearchHistoryORM(db.Model):
    __tablename__ = "search_history"
    history_id = Column(Integer, unique=True, primary_key=True)
    total_string = Column(String(3000))
    word = Column(String(1000))
    typeof = Column(String(45))
    is_benned = Column(Boolean, default=False)
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()