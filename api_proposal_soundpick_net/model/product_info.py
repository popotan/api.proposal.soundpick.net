# _*_ coding: utf-8 _*_
from sqlalchemy import Column, Integer, String, DateTime
from flask_sqlalchemy import SQLAlchemy

from datetime import datetime, timedelta, date

from api_proposal_soundpick_net import db

class ProductInfoORM(db.Model):
    __tablename__ = "product_info"
    product_id = Column(Integer, unique=True, primary_key=True)
    product_name = Column(String(100))
    comment = Column(String(2000))
    _tags = Column('tags', String(1000))
    reg_date = Column(DateTime, default=datetime.utcnow()+timedelta(hours=9))

    @property
    def tags(self):
        return self._tags.replace(' ', '').split('#')[1:]
    
    @tags.setter
    def tags(self, tag):
        self._tags = ' '.join(tags)

    def add(self):
        db.session.add(self)
        return db.session.commit()

    def update(self):
        return db.session.commit()

    def delete(self):
        db.session.delete(self)
        return db.session.commit()

    def rollback(self):
        return db.session.rollback()