from flask import Flask, send_from_directory, session, request, render_template, g
from flask_session import Session
from flask_bcrypt import Bcrypt

from api_proposal_soundpick_net.config import database
from datetime import timedelta

app = Flask(__name__, template_folder='template', static_url_path='/static')

app.secret_key = 'api-proposal-soundpick-net'

app.config.from_object(database)
from flask_sqlalchemy import SQLAlchemy
db = SQLAlchemy()
db.init_app(app)

@app.route('/robots.txt')
def robots():
	return send_from_directory('static/', 'robots.txt')

@app.route('/static/<path:path>')
def send_js(path):
	"""
		static 파일들을 템플릿상에서 url_for 없이 사용하기 위해 라우팅 제공
	"""
	return send_from_directory('static/', path)

from api_proposal_soundpick_net.controller.keyword import keyword
from api_proposal_soundpick_net.controller.product import product
app.register_blueprint(keyword, url_prefix='/keyword')
app.register_blueprint(product, url_prefix='/product')