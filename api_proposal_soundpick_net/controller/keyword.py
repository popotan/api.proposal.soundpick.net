from flask import Blueprint, render_template, session, request, redirect, jsonify

from sqlalchemy import not_, func, desc

from api_proposal_soundpick_net.model.keyword_list import KeywordListORM
from api_proposal_soundpick_net.model.search_history import SearchHistoryORM
from api_proposal_soundpick_net.model.noun_dict import NounDictORM

from api_proposal_soundpick_net.composer.keyword_regex import KeywordResolver
from api_proposal_soundpick_net.composer.query_maker import QueryMaker

keyword = Blueprint('keyword', __name__)

@keyword.route('/', methods=['GET'])
def index():
	keyword = request.args.get('keyword')
	#composer에서 형태소분석기 만들기
	keyword_list = KeywordListORM().query.filter(KeywordListORM.input_keyword.ilike('%' + keyword + '%')).group_by(KeywordListORM.output_keyword).all()
	repr_cols = ['keyword_id', 'input_keyword', 'output_keyword']
	result = [{_col:getattr(_row, _col) for _col in repr_cols} for _row in keyword_list]
	return jsonify(result)

@keyword.route('/history/remove', methods=['POST'])
def save_remove_history():
	sentence = request.get_json()['sentence']
	remove_target = request.get_json()['tag']

	history = SearchHistoryORM().query.filter_by(total_string=sentence).filter_by(word=remove_target['word']).all()
	for t in history:
		t.is_benned = True
		t.update()
	return jsonify({'result' : 'OK'})

@keyword.route('/sentence', methods=['POST'])
def resolve_sentence():
	sentence = request.get_json()['sentence']
	pre_added_tag = request.get_json()['added_tag']
	#composer에서 형태소분석기에 돌리기
	kr = KeywordResolver(sentence)

	#미리 정의된 단어가 있는지 검사하고, 있으면 해당 단어의 인덱스를 보관해놓고 있다가, tokenized_text() 할 때, 해당 인덱스의 단어는 skip하기
	regex_text = kr.target_text
	noun_dict = NounDictORM().query.all()
	noun_list = []
	noun_index_list = []
	for w in noun_dict:
		if w.word in regex_text:
			w2 = {'word' : w.word, 'index' : regex_text.index(w.word)}
			noun_list.append(w2['word'])
			noun_index_list.append(w2['index'])

	sentence_arr = kr.parse_tokenized_text()['tokens']

	sentence_arr2 = []
	remove_target_index = [0, 0] #[시작인덱스, 종료인덱스]
	for x in sentence_arr:
		x = kr.split_info(x)
		if x['index'][0] != 0 and x['index'][0] >= remove_target_index[0] and x['index'][0] <= remove_target_index[1]:
			continue
		elif x['index'][0] in noun_index_list:
			x['word'] = noun_list[noun_index_list.index(x['index'][0])]
			remove_target_index = [x['index'][0], x['index'][0] + len(x['word'])]
		save_search_history(sentence, x)
		sentence_arr2.append(x)
	qm = QueryMaker(sentence_arr2)
	result = qm.stem_analysis()

	#미리 추가한 태그 삽입
	for added in pre_added_tag:
		result.append(added)

	return jsonify(result)

def save_search_history(total_string, tokenized):
	history = SearchHistoryORM()
	history.total_string = total_string
	history.word = tokenized['word']
	history.typeof = tokenized['typeof']
	history.add()

@keyword.route('/popular', methods=['GET'])
def get_popular_keyword():
	history = SearchHistoryORM().query.filter_by(typeof="Noun").filter_by(is_benned=False)
	history = history.filter(not_(SearchHistoryORM.word.ilike('%이어폰%')))
	history = history.filter(not_(SearchHistoryORM.word.ilike('%헤드폰%')))
	history = history.filter(not_(SearchHistoryORM.word.ilike('%헤드셋%')))
	history = history.order_by(desc(func.count(SearchHistoryORM.word)))
	history = history.group_by(SearchHistoryORM.word)
	history = history.limit(10)
	history = history.all()
	repr_cols = ['history_id', 'word']
	result = [{_col: getattr(_row, _col) for _col in repr_cols} for _row in history]
	return jsonify(result)