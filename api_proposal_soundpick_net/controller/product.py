from flask import Blueprint, render_template, session, request, redirect, jsonify

from sqlalchemy import not_, or_

from api_proposal_soundpick_net.model.product_info import ProductInfoORM

product = Blueprint('product', __name__)

@product.route('/proposal', methods=['POST'])
def index():
	if 'values' in request.get_json() and len(request.get_json()['values']) > 0:
		common_conditions = {
			'negative' : [],
			'positive' : []
		}
		query_conditions = {
			'negative' : [],
			'positive' : []
		}
		values = request.get_json()['values']
		for value in values:
			target = '%' + value['word'] + '%'
			if value['is_negative']:
				common_conditions['negative'].append(value)
				query_conditions['negative'].append(not_(ProductInfoORM._tags.ilike(target)))
			else:
				common_conditions['positive'].append(value)
				query_conditions['positive'].append(ProductInfoORM._tags.ilike(target))
				query_conditions['positive'].append(ProductInfoORM.comment.ilike(target))

		product_list = ProductInfoORM().query.filter(or_(*query_conditions['positive']))
		product_list = product_list.all()
		repr_cols = ['product_id', 'product_name', 'comment', 'tags']
		result = [{_col : getattr(_row, _col) for _col in repr_cols} for _row in product_list]
		result = align_by_condition(common_conditions, result)
		return jsonify(result)
	else:
		return ('검색조건이 지정되지 않았습니다.', 204)

def align_by_condition(common_conditions, query_result):
	for product in query_result:
		product['rank'] = 0
		for condition in common_conditions['positive']:
			#태그일치도
			if condition['word'] in product['tags']:
				product['rank'] = product['rank'] + 1
			if condition['word'] in product['comment']:
				product['rank'] = product['rank'] + 1
	print(query_result)
	return query_result


